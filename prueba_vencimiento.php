<?php
// Función para escribir datos en un archivo CSV con el separador |
function escribirCSV($filename, $data, $delimiter = '|') {
    if (($file = fopen($filename, 'w')) !== FALSE) {
        foreach ($data as $row) {
            fputcsv($file, $row, $delimiter);
        }
        fclose($file);
        echo "Archivo '$filename' generado correctamente.\n";
    } else {
        echo "Error al abrir el archivo '$filename' para escritura.\n";
    }
}

// Función para leer el archivo CSV y obtener los datos
function obtenerDatosCSV($filename, $delimiter = '|') {
    $data = [];
    if (($handle = fopen($filename, 'r')) !== FALSE) {
        // Saltar la primera línea (encabezado)
        fgetcsv($handle, 1000, $delimiter);
        // Leer las líneas restantes
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
            $data[] = $row[0]; // Obtener el dato del índice 0
        }
        fclose($handle);
    } else {
        echo "Error al abrir el archivo '$filename' para lectura.\n";
    }
    return $data;
}

// Función para generar períodos en base a la primera letra del dominio
function generarPeriodos($startDate, $meses, $letra) {
    $diasPorLetra = ord(strtoupper($letra)) - ord('A');
    $fechaInicio = strtotime("+$diasPorLetra days", strtotime($startDate));
    $periodos = [];

    for ($i = 0; $i < $meses; $i++) {
        $fechaFin = strtotime('+30 days', $fechaInicio);
        $periodos[] = date('d.m.Y', $fechaInicio) . ' - ' . date('d.m.Y', $fechaFin);
        $fechaInicio = strtotime('+1 month', $fechaInicio);
    }

    return $periodos;
}

// Datos para la tabla de períodos y dominios
function generarPeriodoDominio($vehiculos, $startDate) {
    $periodoDominioData = [["ID", "Dominio", "Periodo 1", "Periodo 2", "Periodo 3", "Periodo 4", "Periodo 5"]];
    $id = 1;

    foreach ($vehiculos as $dominio) {
        $periodos = generarPeriodos($startDate, 5, $dominio[0]);
        $periodoDominioData[] = array_merge([$id++, $dominio], $periodos);
    }

    return $periodoDominioData;
}

// Generar y escribir los archivos CSV
$vehiculos = obtenerDatosCSV('Vehiculos.csv');
$startDate = '10.01.2024';

$periodoDominioData = generarPeriodoDominio($vehiculos, $startDate);
escribirCSV('Periodo_Dominio.csv', $periodoDominioData);

?>
