<?php
require('fpdf/fpdf.php');

// Datos para la tabla
$data = [
    ["0", "350.000", "0", "3,305"],
    ["350.000", "420.000", "11.568", "4,446"],
    ["420.000", "490.000", "14.680", "4,982"],
    ["490.000", "560.000", "18.167", "5,298"],
    ["560.000", "630.000", "21.876", "5,488"],
    ["630.000", "700.000", "25.718", "5,584"],
    ["700.000", "770.000", "29.627", "5,723"],
    ["770.000", "840.000", "33.633", "5,805"],
    ["840.000", "910.000", "37.697", "5,928"],
    ["910.000", "980.000", "41.847", "6,006"],
    ["980.000", "1.050.000", "46.051", "6,085"],
    ["1.050.000", "1.190.000", "50.311", "6,178"],
    ["1.190.000", "1.400.000", "58.960", "6,284"],
    ["1.400.000", "8.000.000", "72.156", "6,323"],
    ["8.000.000", "", "489.474", "6,370"],
];

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 10);

// Títulos de las columnas
$header = ["Base Imponible ($)", "Cuota fija ($)", "Alicuota s/ excedente limite minimo %"];
$subheader = ["Mayor a", "Menor igual a"];
$w = [30, 30, 60, 70]; // Ancho de las columnas

// Cabecera
$pdf->Cell($w[0] + $w[1], 10, $header[0], 1, 0, 'C');
$pdf->Cell($w[2], 10, $header[1], 1, 0, 'C');
$pdf->Cell($w[3], 10, $header[2], 1, 0, 'C');
$pdf->Ln();

// Subcabecera
$pdf->Cell($w[0], 10, $subheader[0], 1, 0, 'C');
$pdf->Cell($w[1], 10, $subheader[1], 1, 0, 'C');
$pdf->Cell($w[2], 10, '', 0, 0, 'C'); // Espacio en blanco bajo "Cuota fija"
$pdf->Cell($w[3], 10, '', 0, 0, 'C'); // Espacio en blanco bajo "Alicuota"
$pdf->Ln();

// Datos
$pdf->SetFont('Arial', '', 12);
foreach($data as $row) {
    $pdf->Cell($w[0], 10, $row[0], 1);
    $pdf->Cell($w[1], 10, $row[1], 1);
    $pdf->Cell($w[2], 10, $row[2], 1, 0, 'R');
    $pdf->Cell($w[3], 10, $row[3], 1, 0, 'R');
    $pdf->Ln();
}

$pdf->Output('F', 'tabla_alicuotas.pdf');
echo "PDF generado correctamente.";
?>
