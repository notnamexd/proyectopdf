<?php
require('fpdf/fpdf.php');

// Función para leer el archivo CSV y obtener los datos
function obtenerDatosCSV($filename) {
    $data = [];
    if (($handle = fopen($filename, "r")) !== FALSE) {
        // Saltar la primera línea (encabezado)
        fgetcsv($handle, 1000, "|");
        // Leer las líneas restantes
        while (($row = fgetcsv($handle, 1000, "|")) !== FALSE) {
            $data[] = $row[0]; // Obtener el dato del índice 0
        }
        fclose($handle);
    }
    return $data;
}

// Datos del archivo CSV
$vehiculos = obtenerDatosCSV('Vehiculos.csv');

// Periodos
$periodos = [
    "1 periodo" => "15.01.2011 - 15.02.2011",
    "2 periodo" => "15.02.2011 - 15.03.2011",
    "3 periodo" => "15.03.2011 - 15.04.2011",
    "4 periodo" => "15.04.2011 - 15.05.2011",
    "5 periodo" => "15.05.2011 - 15.06.2011"
];

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 8);

// Títulos de las columnas
$pdf->Cell(20, 10, 'Dominio', 1);
$pdf->Cell(165, 10, 'Periodos', 1);
$pdf->Ln();

// Subcabecera para los periodos
$pdf->Cell(20, 10, '', 0); // Espacio en blanco bajo "Dominio"
foreach (array_keys($periodos) as $periodo) {
    $pdf->Cell(33, 10, $periodo, 1);
}
$pdf->Ln();

// Datos
$pdf->SetFont('Arial', '', 8);
foreach ($vehiculos as $dominio) {
    // Columna de dominio
    $pdf->Cell(20, 10, $dominio, 1);
    // Columnas de periodos
    foreach ($periodos as $dato) {
        $pdf->Cell(33, 10, $dato, 1);
    }
    $pdf->Ln();
}

$pdf->Output('F', 'Periodo_dominio.pdf');
echo "PDF generado correctamente.";
?>
